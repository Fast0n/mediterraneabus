import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cerca',
      theme: ThemeData(
          primaryColor: Color(0xFF0095ea),
          accentColor: Color(0xFF0095ea),
          brightness: Brightness.light),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Cerca'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  _showToast(String message) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      backgroundColor: Colors.grey,
      textColor: Colors.white,
    );
  }

  _showAlert(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Row(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 10.0),
                  child: GestureDetector(
                    onTap: () {},
                    child: Icon(
                      Icons.warning,
                      size: 26.0,
                      color: Colors.red,
                    ),
                  )),
              new Text('  Attenzione')

              //Row(children: [Icon(Icons.warning), ]),
            ],
          ),
          content: Text(
              "L\'app è stata creata in modo non ufficiale MEDITERRANEABUS SPA. non è responsabile in alcun modo"),
          actions: <Widget>[
            FlatButton(
              child: Text('Ho letto e accetto le condizioni'.toUpperCase()),
              onPressed: () {
                //Put your code here which you want to execute on Yes button click.
                Navigator.of(context).pop();
                _showChangelog(context);
              },
            ),
          ],
        );
      },
    );
  }

  _showChangelog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Row(children: [Text('Novità')]),
          content: Text("Versione 0.4\nProva"),
          actions: <Widget>[
            FlatButton(
              child: Text('Chiudi'.toUpperCase()),
              onPressed: () {
                //Put your code here which you want to execute on Yes button click.
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xFF0095ea),
    ));

    return Scaffold(
        appBar: AppBar(
            elevation: 0,
            title: Stack(
              children: <Widget>[
                new Container(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: GestureDetector(
                              onTap: () {},
                              child: Icon(
                                Icons.directions_bus,
                                size: 26.0,
                              ),
                            )),
                        Text(widget.title, textAlign: TextAlign.center)
                      ],
                    ),
                  ],
                )),
                new Container(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(right: 20.0),
                            child: GestureDetector(
                              onTap: () => _showToast("Cosa vuoi di più"),
                              child: Icon(
                                Icons.info_outline,
                                size: 26.0,
                              ),
                            )),
                        GestureDetector(
                          onTap: () {},
                          child: Icon(Icons.more_vert),
                        )
                      ],
                    ),
                  ],
                ))
              ],
            )),
        body: Stack(
          children: <Widget>[
            SafeArea(
                child: Column(
              children: <Widget>[
                Container(
                    padding: EdgeInsets.only(left: 20, top: 10.0, right: 20, bottom: 10.0),
                    color: Color(0xFF0095ea),
                    child: Container(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Card(
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      ButtonTheme(
                                        height: 50,
                                        child: RaisedButton(
                                            elevation: 0,
                                            textColor: Colors.grey,
                                            color: Colors.white,
                                            onPressed: () =>
                                                _showAlert(context),
                                            child: Container(
                                              alignment: Alignment.centerLeft,
                                              child: Text('Partenza'),
                                            )),
                                      ),

                                      // line
                                      Container(
                                        height: 1.5,
                                        margin: EdgeInsets.only(left: 10),
                                        color: Colors.grey,
                                      ),

                                      ButtonTheme(
                                        height: 50,
                                        child: RaisedButton(
                                            elevation: 0,
                                            textColor: Colors.grey,
                                            color: Colors.white,
                                            onPressed: () =>
                                                _showAlert(context),
                                            child: Container(
                                              alignment: Alignment.centerLeft,
                                              child: Text('Arrivo'),
                                            )),
                                      )
                                    ],
                                  ),
                                ),
                                ButtonTheme(
                                    height: 100,
                                    minWidth: 40,
                                    child: RaisedButton(
                                      elevation: 0,
                                      textColor: Colors.black,
                                      color: Colors.white,
                                      onPressed: () => _showAlert(context),
                                      child: new Icon(
                                        Icons.autorenew,
                                        color: Colors.blue,
                                      ),
                                    ))
                              ],
                            ),
                          ),
                          Card(
                              color: Colors.white,
                              margin: EdgeInsets.only(top: 10),
                              child: ButtonTheme(
                                  height: 10,
                                  minWidth: double.infinity,
                                  child: RaisedButton(
                                      elevation: 0,
                                      textColor: Colors.black,
                                      color: Colors.white,
                                      onPressed: () => _showAlert(context),
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    right: 20.0),
                                                child: Icon(
                                                  Icons.today,
                                                  color: Colors.blue,
                                                )),
                                            Text("Scolastico"),
                                          ],
                                        ),
                                      )))

                              //
                              ),
                          Container(
                              padding: EdgeInsets.only(top: 10),
                              child: ButtonTheme(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.horizontal(
                                          left: Radius.circular(30.0),
                                          right: Radius.circular(30.0))),
                                  height: 50,
                                  minWidth: double.infinity,
                                  child: RaisedButton(
                                    elevation: 0,
                                    textColor: Colors.black,
                                    color: Colors.white,
                                    onPressed: () => _showAlert(context),
                                    child: new Text("Cerca"),
                                  )))
                        ],
                      ),
                    ))
              ],
            ))
          ],
        ));
  }
}
